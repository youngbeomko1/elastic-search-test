from flask import Flask, render_template, request, session, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
import os, base64, re, logging, requests, json, pymysql

from flask.ext.mongoengine import MongoEngine
from mongoengine import connect, Document
from bson.objectid import ObjectId

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import InputRequired, Length, Email, ValidationError

from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from elasticsearch_dsl.query import MultiMatch, Match

from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager, UserMixin, login_user, logout_user, current_user

app = Flask(__name__)

# mysql database login
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://sql9225892:uf5i3D9ZTI@sql9.freemysqlhosting.net/sql9225892'
mysql_db = SQLAlchemy(app)

app.config['SECRET_KEY'] = 'SomeSecretKeyIgnoreSinceItsDemo!'

# mongo db login
app.config["MONGODB_DB"] = 'elastic-mongodb'
connect(
    'elastic-mongodb',
    username='young',
    password='young',
    host='mongodb://young:young@ds163918.mlab.com:63918/elastic-mongodb',
    port=63918
)
m_db = MongoEngine(app)

login_manager = LoginManager()
login_manager.init_app(app)


# login validation
class LoginForm(FlaskForm):
    username = StringField('username', validators=[InputRequired('A username is required!'), Length(min=4, max=20, message='Must be between 4 and 20 letters.')])
    password = PasswordField('password', validators=[InputRequired('Password is required!') ])


# Register validation
class RegisterForm(FlaskForm):
    username = StringField('username', validators=[InputRequired('A username is required!'), Length(min=4, max=20, message='Must be between 4 and 20 letters.')])
    password = PasswordField('password', validators=[InputRequired('Password is required!') ])
    first_name = StringField('first_name', validators=[InputRequired('First name is required!'), Length(max=20, message='Must be 20 or less letters.') ])
    last_name = StringField('last_name', validators=[InputRequired('Last name is required!'), Length(max=20, message='Must be 20 or less letters.') ])
    email = StringField('email', validators=[InputRequired('Email is required!'), Length(max=50, message='Must be 20 or less letters.'), Email('Enter right email format.') ])


# User account schema
class user_account(UserMixin, mysql_db.Model):
    __tablename__ = 'user_account'
    id = mysql_db.Column('user_id', mysql_db.Integer, primary_key = True)
    username = mysql_db.Column('username', mysql_db.String(20))
    password = mysql_db.Column('password' ,mysql_db.String(80))
    first_name = mysql_db.Column('first_name', mysql_db.String(20))
    last_name = mysql_db.Column('last_name', mysql_db.String(20))
    email = mysql_db.Column('email', mysql_db.String(50))


# mongodb schema
class books(m_db.Document):
    title = m_db.StringField(required=True)
    isbn = m_db.StringField()
    pageCount = m_db.StringField()
    publishedDate = m_db.StringField()
    thumbnailUrl = m_db.StringField()
    shortDescription = m_db.StringField()
    longDescription = m_db.StringField()
    status = m_db.StringField()
    authors = m_db.StringField()
    categories = m_db.StringField()


# login for bonsai elastic search server
bonsai = 'https://o4mmchjcpj:edizapwh7l@banyan-8312058.us-east-1.bonsaisearch.net'
auth = re.search('https\:\/\/(.*)\@', bonsai).group(1).split(':')
host = bonsai.replace('https://%s:%s@' % (auth[0], auth[1]), '')

es_header = [{
  'host': host,
  'port': 443,
  'use_ssl': True,
  'http_auth': (auth[0],auth[1]),
  'verify_certs': False
}]

es = Elasticsearch(es_header)

@login_manager.user_loader
def load_user(user_id):
    return user_account.query.get(int(user_id))


# if user authenticated route to search, if not route ot login
@app.route("/")
def home():
    if not current_user.is_authenticated:
        return redirect(url_for('login'))

    return redirect(url_for('search'))


# login users, if already logged in logout
@app.route("/login", methods=['POST', 'GET'])
def login():

    form = LoginForm()
    error = None

    # if form validate start authenticate process
    if form.validate_on_submit():
        user = user_account.query.filter_by(username=form.username.data).first()
        # check if user exist
        if user:
            # check if password 
            if check_password_hash(user.password, form.password.data):
                login_user(user)
                print('pass')
                return redirect(url_for('search'))
            else:
                error = 'Invalid username or Password'
                return render_template('login.html', form=form, error=error)
        else:
            error = 'Invalid Username or Password'
            return render_template('login.html', form=form, error=error)      

    return render_template('login.html', form=form)

# register new user
@app.route("/register", methods=['POST', 'GET'])
def register():

    form = RegisterForm()
    error = None

    # if form validates save
    if form.validate_on_submit():
        # check username and email is not duplicate
        user_username = user_account.query.filter_by(username=form.username.data).first()
        user_email = user_account.query.filter_by(email=form.email.data).first()
        if user_email or user_username:
            error = 'Username and Email is already registered'
            if not user_email:
                error = 'Username is being used'
            if not user_username:
                error = 'Email already exist'
            return render_template('register.html', form=form, error=error)
        else:
            # hash password and create user
            hashed_password = generate_password_hash(form.password.data, method='sha256')
            new_user = user_account(username=form.username.data, password=hashed_password, first_name=form.first_name.data, last_name=form.last_name.data, email=form.email.data)
            mysql_db.session.add(new_user)
            mysql_db.session.commit()

            # check if user has been created, if true route to login, if false render registration with error
            if user_account.query.filter_by(username=form.username.data).first():
                return redirect(url_for('login'))
            else:
                error = 'Sorry you data could not be saved'
                return render_template('register.html', form=form, error=error)

    return render_template('register.html', form=form)

#logout user and route to login page
@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('login'))

#search result
@app.route("/search")
def search():
    if not current_user.is_authenticated:
        return redirect(url_for('login'))

    data = None
    error = None

    # get data doe search and pagination
    search_value = request.args.get('search_value')
    page = request.args.get('page')

    if search_value:
        r = requests.get(bonsai)

        # check for elasticseach server
        if r.status_code == 200:
            #query elastic search
            s = Search(using=es)
            data = s.query('multi_match', query=str(search_value), fields=['categories', 'title', 'authors', 'publishedData','shortDescription', 'longDescription' ])
            
            if data.count() == 0:
                error = "Sorry no results, try searching other things"
                return render_template('search.html', error = error)

            # paginate
            total = data.count()
            try:
                int(page)
            except:
                page = 1

            page =int(page)

            if (data.count()/5) < page:
                f_page = (data.count()/5) * 5 - 1
                if f_page < 0:
                    f_page = 0
            elif page > 1:
                f_page = (page - 1) * 5
            else:
                f_page = 0
                page = 1

            data = data[f_page:(f_page+5)].execute()

            return render_template('search.html', data = data, page = page, total = total, max_page = (total/5)+1, search_value = search_value)


        else: 
            error = " unable to connect to elasticsearch server try again"
            return render_template('search.html', error = error)

        
    
    return render_template('search.html')

#show search result by searchin mongodb for id
@app.route("/result")
def result():
 
    result_value = request.args.get('id')
    item = None

    if result_value:
        ids = None
        #check if value is hex key and if object with key exist
        try:
            ids = ObjectId(str(result_value))
            item = books.objects.get(id = ids)
        except:
            return redirect(url_for('search'))

        ids = ObjectId(str(result_value))  
        item = books.objects.get(id = ids)

        return render_template('result.html', data = item)
    
    return redirect(url_for('search'))
    #return render_template('result.html', data = item)


# index data from mongodb to elastic search server
@app.route('/elastic')
def elastic():
    r = requests.get(bonsai)

    if r.status_code == 200:

        for obj in books.objects:
            data = obj
            json_data = obj.to_json()

            # temporay fix for _id conflict with elastic search 
            json_data = json_data.replace('_id', 'id')

            es.index(index='books', doc_type='books', id = str(data.id), body = json_data)
    else:
        return 'can not connect to server try again'

    return 'indexed data in mongoDB to elastic search.'



if __name__ == "__main__":
    app.run(debug=True)
